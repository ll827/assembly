#!/usr/bin/perl
use POSIX;

# Helper subroutine for indexing 9-mers in the input reads
# $read: Process this single read
# $read_index: Unique ID of this read based on order in which it was read from file
# \%merloc: Pointer to hash of 9-mer locations
sub merIndex {
	my $read = $_[0];
	my $read_index = $_[1];
	my $merloc = $_[2]; # Actual type %
	
	# This is essentially a pointer that runs along the read and records the sequence
	# identity at the current location
	my @current_mer = ();

	my @read_array = split(//, $read);

	# Tracks the basepair at which we're currently looking at
	for (my $pos = 0; $pos < length($read); $pos++) {
		my $char = $read_array[$pos];
		push(@current_mer, $char);

		# Length check
		if (scalar(@current_mer) > 9) {
			shift(@current_mer);
		}

		my $current_mer_to_string = join('',@current_mer);

		if (length($current_mer_to_string) == 9) {
			if (exists(${$merloc}{$current_mer_to_string})) { # Extension
				my @list = ($read_index, $pos-8);
				push(@{${$merloc}{$current_mer_to_string}}, \@list);
			} else { # Instantiation
				my @list = ($read_index, $pos-8);
				my @list2 = (\@list);
				${$merloc}{$current_mer_to_string} = \@list2;
			}
		}
	}
}
# Helper subroutine responsible for starting from the head of the current assembly
# and finding the unique matching extension read
# @param
# query_id: Read that will be queried against the other reads
# direction: '5' or '3' to indicate which direction to search
# \@reads: Access to read sequences
# \%seen: Which reads have already been incorporated into the assembly
# \%merloc: Locations of mers
# \$assembly: The string with the final assembly
# @return
# Returns the ID of the read that was successfully matched, -1 otherwise
sub readExtension {
	my $query_id = $_[0];
	my $direction = $_[1];
	my $reads = $_[2]; # Actual type @
	my $seen = $_[3]; # Actual type %
	my $merloc = $_[4]; # Actual type %
	my $assembly = $_[5]; # Actual type $
	my $cutoff = $_[6];
	
	my $query_read = @{$reads}[$query_id];
	
	# Where are we going to extract the query k-mer?
	my $query_pos;

	# Initialize $query_kmer
	if ($direction == 3) {
		$query_pos = ceil(length($query_read)/2) - 1;
	} elsif ($cutoff) {
		$query_pos = int(length($query_read)/2) - 8;
	} else {
		$query_pos = 0;
	}
	
	my @candidates;
	
	if ($cutoff) {
		$query_kmer = substr($query_read, $query_pos, 9);
		@candidates = @{${$merloc}{$query_kmer}};
	} else {
		$query_kmer = substr($query_read, $query_pos, ceil(length($query_read)/2));
		@candidates = @$reads;
	}
	
	# Keeps track of whether a matching read was found
	my $matched = -1;
	
	# Process each candidate
	for (my $i = 0; $i < scalar(@candidates); $i++) {
	
		my $cand_id;
		my $cand_pos;
		my $cand_seq;
	
		if (!$cutoff) {
		
			$cand_id = $i;
		
			# Skip if this read has already been incorporated into the assembly
			if (exists(${$seen}{$cand_id})) {
				next;
			}
		
			$cand_seq = $candidates[$i];
			if (($cand_pos = index($cand_seq, $query_kmer)) == -1) {
				next;
			}
			
		} else {
			$cand_id = @{$candidates[$i]}[0];
			$cand_pos = @{$candidates[$i]}[1];
			$cand_seq = ${$reads}[$cand_id];
		
			# Skip if this read has already been incorporated into the assembly
			if (exists(${$seen}{$cand_id})) {
				next;
			}
		
			if ((direction == 3 && $cand_pos >= ceil(length($cand_seq)/2 - 1)) || 
					(direction == 5 && $cand_pos <= int(length($cand_seq)/2) - 8)) {
				next;
			}
		}
		
		# Bidirectional walking
		my @query_seq_array = split(//, $query_read);
		my @cand_seq_array = split(//, $cand_seq);

		my $query_pointer = $query_pos;
		my $cand_pointer = $cand_pos;
	
		# 5' direction
		while ($cand_pointer >= 0 && $query_pointer >= 0) {
			if ($query_seq_array[$query_pointer] != $cand_seq_array[$cand_pointer]) {
				last;
			}
			$query_pointer--;
			$cand_pointer--;
		}
	
		# Disqualification
		if ($cand_pointer >= 0 && $query_pointer >= 0) {
			next;
		}
	
		# 3' direction
		while ($query_pointer < length($query_read) && $cand_pointer < length($cand_seq)) {
			if ($query_seq_array[$query_pointer] != $cand_seq_array[$cand_pointer]) {
				last;
			}
			$query_pointer++;
			$cand_pointer++;
		}
	
		# Disqualification
		if ($query_pointer < length($query_read) && $cand_pointer < length($cand_seq)) {
			next;
		}
	
		# This is the one
		# Integrate into $assembly
		if ($direction == 3) {
			$$assembly .= substr($cand_seq, $cand_pos + (length($query_read) - $query_pos));
		} else { # direction == 5
			$$assembly = substr($cand_seq, 0, $cand_pos-$query_pos).$$assembly;
		}
	
		$matched = $cand_id;
		${$seen}{$cand_id} = 1;
		last;
	}
	
	return $matched;
}

# Command line arguments
# Input FASTA file
my $infile;

if (scalar(@ARGV) != 1) {
	print "Usage: simple_assembler.pl [input file]\n";
	exit(1);
} else {
	$infile = shift(@ARGV);
}

# Internal data representations

# Input reads
my @reads;

# Read index
my $read_index = 0;

# Hash table of 9-mer locations
# Shoutout to all the Warcraft fans
my %merloc;

# The current read whose extension we seek
my $query_read;

# The query k-mer
my $query_kmer;

# Assembled sequence
my $assembly = "";

# Hash of sequence IDs that have already been incorporated into the $assembly
my %seen;

# Is this the first read that we look at?
my $first = 1;

# Length cutoff for the seed
my $cutoff = 1;

# For multi-line reads, must keep track of the entire string
my $single_read = "";

open INFILE, "<$infile" or die "Can't open $infile: $!\n";

while (my $line = <INFILE>) {
	chomp($line);
	
	if (index($line, ">") == -1) {
		$single_read .= $line;
	} else {
	
		if ($single_read ne "") {
	
			push(@reads, $single_read);
	
			if ($first) { 
				if (length($single_read) < 16) {
					$cutoff = 0;
				} else {
					$cutoff = 1;
				}
			}
			$first = 0;
	
			if ($cutoff) { # Only do the seed code if the read length is large enough
				merIndex($single_read, $read_index, \%merloc);
			}
			$single_read = "";
			$read_index++;
		}
	}
}

# Fencepost
push(@reads, $single_read);
if ($cutoff) { # Only do the seed code if the read length is large enough
	merIndex($single_read, $read_index, \%merloc);
}

# Initial parameters for read extension
my $query_id = 0;
my $direction = 3;
$assembly = $reads[$query_id];
$seen{$query_id} = 1;

while (scalar(keys(%seen)) < scalar(@reads)) {
	
	if ($query_id == -1) {
		$query_id = 0;
		$direction = 5;
	}
	
	$query_id = readExtension($query_id, $direction, \@reads, \%seen, \%merloc, \$assembly, $cutoff);
}

# Print the final answer to standard output. User can redirect as needed.
print $assembly."\n";
exit();
